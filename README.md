# OpenML dataset: MiceProtein

https://www.openml.org/d/4550

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Clara Higuera, Katheleen J. Gardiner, Krzysztof J. Cios  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Mice+Protein+Expression) - 2015   
**Please cite**: Higuera C, Gardiner KJ, Cios KJ (2015) Self-Organizing Feature Maps Identify Proteins Critical to Learning in a Mouse Model of Down Syndrome. PLoS ONE 10(6): e0129126.

**Deactivated because the attributes Genotype, Treatment and Behavior should be ignored. They leak information about the target. Use version 4 instead.** 

Expression levels of 77 proteins measured in the cerebral cortex of 8 classes of control and Down syndrome mice exposed to context fear conditioning, a task used to assess associative learning.

### Source:
```
Clara Higuera Department of Software Engineering and Artificial Intelligence, Faculty of Informatics and the Department of Biochemistry and Molecular Biology, Faculty of Chemistry, University Complutense, Madrid, Spain. 
Email: clarahiguera '@' ucm.es 

Katheleen J. Gardiner, creator and owner of the protein expression data, is currently with the Linda Crnic Institute for Down Syndrome, Department of Pediatrics, Department of Biochemistry and Molecular Genetics, Human Medical Genetics and Genomics, and Neuroscience Programs, University of Colorado, School of Medicine, Aurora, Colorado, USA. 
Email: katheleen.gardiner '@' ucdenver.edu 

Krzysztof J. Cios is currently with the Department of Computer Science, Virginia Commonwealth University, Richmond, Virginia, USA, and IITiS Polish Academy of Sciences, Poland. 
Email: kcios '@' vcu.edu 
```

### Data Set Information

The data set consists of the expression levels of 77 proteins/protein modifications that produced detectable signals in the nuclear fraction of cortex. There are 38 control mice and 34 trisomic mice (Down syndrome), for a total of 72 mice. In the experiments, 15 measurements were registered of each protein per sample/mouse. Therefore, for control mice, there are 38x15, or 570 measurements, and for trisomic mice, there are 34x15, or 510 measurements. The dataset contains a total of 1080 measurements per protein. Each measurement can be considered as an independent sample/mouse. 

The eight classes of mice are described based on features such as genotype, behavior and treatment. According to genotype, mice can be control or trisomic. According to behavior, some mice have been stimulated to learn (context-shock) and others have not (shock-context) and in order to assess the effect of the drug memantine in recovering the ability to learn in trisomic mice, some mice have been injected with the drug and others have not. 

Classes: 
```
* c-CS-s: control mice, stimulated to learn, injected with saline (9 mice) 
* c-CS-m: control mice, stimulated to learn, injected with memantine (10 mice) 
* c-SC-s: control mice, not stimulated to learn, injected with saline (9 mice) 
* c-SC-m: control mice, not stimulated to learn, injected with memantine (10 mice) 
* t-CS-s: trisomy mice, stimulated to learn, injected with saline (7 mice) 
* t-CS-m: trisomy mice, stimulated to learn, injected with memantine (9 mice) 
* t-SC-s: trisomy mice, not stimulated to learn, injected with saline (9 mice) 
* t-SC-m: trisomy mice, not stimulated to learn, injected with memantine (9 mice) 
```

The aim is to identify subsets of proteins that are discriminant between the classes. 

### Attribute Information:

```
1 Mouse ID 
2..78 Values of expression levels of 77 proteins; the names of proteins are followed by &acirc;&euro;&oelig;_n&acirc;&euro; indicating that they were measured in the nuclear fraction. For example: DYRK1A_n 
79 Genotype: control (c) or trisomy (t) 
80 Treatment type: memantine (m) or saline (s) 
81 Behavior: context-shock (CS) or shock-context (SC) 
82 Class: c-CS-s, c-CS-m, c-SC-s, c-SC-m, t-CS-s, t-CS-m, t-SC-s, t-SC-m 
```

### Relevant Papers:

Higuera C, Gardiner KJ, Cios KJ (2015) Self-Organizing Feature Maps Identify Proteins Critical to Learning in a Mouse Model of Down Syndrome. PLoS ONE 10(6): e0129126. [Web Link] journal.pone.0129126 

Ahmed MM, Dhanasekaran AR, Block A, Tong S, Costa ACS, Stasko M, et al. (2015) Protein Dynamics Associated with Failed and Rescued Learning in the Ts65Dn Mouse Model of Down Syndrome. PLoS ONE 10(3): e0119491.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4550) of an [OpenML dataset](https://www.openml.org/d/4550). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4550/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4550/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4550/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

